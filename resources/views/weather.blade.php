<!DOCTYPE html>
<html lang="en">
<head>
    <title>Weather Task</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Weather AVG</h2>
    <form action="{{route('weather.store')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group">
            <label for="country">Country Code (2 characters):</label>
            <input type="text" class="form-control" id="country" placeholder="Enter country" name="country" required>
        </div>
        <div class="form-group">
            <label for="city">City Name:</label>
            <input type="text" class="form-control" id="city" placeholder="Enter city" name="city" required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
        @if(\Illuminate\Support\Facades\Session::has('avg'))
            <div class="form-group">
                <label for="city">The Average Temperature for the next 10 days is :</label>
                <input type="text" class="form-control"  value="{{\Illuminate\Support\Facades\Session::get('avg')}}" readonly >
            </div>
        @endisset
    </form>
</div>

</body>
</html>
