<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WeatherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('weather');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $this->validate($request, ['country'=>'required|max:2','city'=>'required']);
        $key = strtolower($params['city']).'-'.strtolower($params['city']);
        $avg = Cache::get($key);
        if (  $avg == null ){
            $avg = $this->getWeatherTemperature($params['country'],$params['city'], $key);
        }
        return redirect()->back()->with(['avg'=>$avg]);
    }


    private function getWeatherTemperature( $country, $city , $key ){
        $response = json_decode(file_get_contents(
            "http://api.weatherbit.io/v2.0/forecast/daily?key=".env('WEATHER_KEY')."&days=10&city=$city&country=$country"));
        try{
            $avg = 0;
            foreach ( $response->data as $data ){
                $avg += $data->temp;
            }
            $avg /= 10;
        }catch ( \Exception $e ){
            $avg = 'N/A';
        }
        Cache::put($key, $avg , now()->addHours(2));
        return $avg;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
